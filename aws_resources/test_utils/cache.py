import aws_resources.dynamodb_table
import aws_resources.sns


def clear_cached_boto_resources():
    aws_resources.dynamodb_table.cached_dynamo_resource = None
    aws_resources.sns.cached_sns_client = None
