from contextlib import contextmanager
from typing import List, Dict
from unittest import TestCase

from aws_resources.aws_resource import GlobalPermissionUseRecorder, PermissionUse


class BaseHandlerPermissionsTestCase(TestCase):
    handler_name = None
    all_permission_uses: List[PermissionUse] = []
    should_write_permissions = False

    @classmethod
    def _write_permissions_used(cls):
        uses_by_resource: Dict[str, List[str]] = {}
        for use in cls.all_permission_uses:
            if use.aws_resource.name not in uses_by_resource:
                uses_by_resource[use.aws_resource.name] = []

            uses_by_resource[use.aws_resource.name].append(use.permission_name)

        with open(f"{cls.handler_name}_permissions.yml", "w") as f:
            for resource_name, permissions in uses_by_resource.items():
                iam_statement = "- Effect: Allow\n  Action:\n"
                iam_statement += "".join([f"    - {permission}\n" for permission in permissions])
                iam_statement += f"  Resource: {resource_name}\n"
                f.write(iam_statement)

    @classmethod
    def tearDownClass(cls) -> None:
        if cls.should_write_permissions:
            cls._write_permissions_used()

    @contextmanager
    def record_permissions(self):
        # Clear the instance first to ensure there aren't any permission uses from previous actions
        GlobalPermissionUseRecorder.clear_instance()
        GlobalPermissionUseRecorder.get_instance()

        yield

        self.all_permission_uses += GlobalPermissionUseRecorder.get_instance().permission_uses
        GlobalPermissionUseRecorder.clear_instance()

    def assertNoPermissionsUsed(self):
        self.assertEqual(0, len(GlobalPermissionUseRecorder.get_instance().permission_uses))

    def assertPermissionsUsed(self, permission_uses: List[PermissionUse]):
        unique_actual_permission_uses = GlobalPermissionUseRecorder.get_instance().unique_permission_uses

        friendly_expected_permission_uses = "\n".join([str(use) for use in permission_uses])
        friendly_actual_permission_uses = "\n".join([str(use) for use in unique_actual_permission_uses])

        self.assertEqual(
            permission_uses,
            unique_actual_permission_uses,
            f"\nExpected:\n{friendly_expected_permission_uses}\n\nActual:\n{friendly_actual_permission_uses}",
        )
