from contextlib import contextmanager
from unittest.mock import Mock, patch


def _create_batch_writer_mock(mock):
    @contextmanager
    def _mock_batch_writer():
        yield mock

    return _mock_batch_writer


@contextmanager
def patch_table(mock_table=None):
    """Patch boto3 and yield the mock Table

    :param mock_table: The Mock to be returned from the DynamoDB resource's Table function
    """
    mock_batch_writer = Mock(
        delete_item=Mock(return_value="batch_writer_delete_item_return_value"),
        put_item=Mock(return_value="batch_writer_put_item_return_value"),
    )

    mock_table_to_use = (
        mock_table
        if mock_table is not None
        else Mock(
            batch_writer=_create_batch_writer_mock(mock_batch_writer),
            delete_item=Mock(return_value="delete_item_return_value"),
            get_item=Mock(return_value="get_item_return_value"),
            put_item=Mock(return_value="put_item_return_value"),
            query=Mock(return_value="query_return_value"),
            scan=Mock(return_value="scan_return_value"),
            update_item=Mock(return_value="update_item_return_value"),
        )
    )
    mock_boto3 = Mock(resource=Mock(return_value=Mock(Table=Mock(return_value=mock_table_to_use))))

    with patch("aws_resources.dynamodb_table.boto3", mock_boto3):
        yield mock_table_to_use, mock_batch_writer


@contextmanager
def patch_sns(mock_sns=None):
    """Patch boto3 and yield the mock SNS client

    :param mock_sns: The Mock to be returned from boto3.client
    """
    mock_sns_to_use = mock_sns if mock_sns else Mock(publish=Mock(return_value={"MessageId": "mock_message_id"}))
    mock_boto3 = Mock(client=Mock(return_value=mock_sns_to_use))

    with patch("aws_resources.sns.boto3", mock_boto3):
        yield mock_sns_to_use
