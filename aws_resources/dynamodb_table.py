import os
from contextlib import contextmanager

import boto3

from aws_resources.aws_resource import AWSResource
from logger import get_logger

logger = get_logger(__name__)

cached_dynamo_resource = None


class DynamoDBTable(AWSResource):
    PERMISSION_PREFIX = "dynamodb"
    BATCH_WRITE_ITEM_PERMISSION = f"{PERMISSION_PREFIX}:BatchWriteItem"
    DELETE_ITEM_PERMISSION = f"{PERMISSION_PREFIX}:DeleteItem"
    GET_ITEM_PERMISSION = f"{PERMISSION_PREFIX}:GetItem"
    PUT_ITEM_PERMISSION = f"{PERMISSION_PREFIX}:PutItem"
    QUERY_PERMISSION = f"{PERMISSION_PREFIX}:Query"
    SCAN_PERMISSION = f"{PERMISSION_PREFIX}:Scan"
    UPDATE_ITEM_PERMISSION = f"{PERMISSION_PREFIX}:UpdateItem"

    def __init__(self, table_name: str):
        super().__init__(table_name)

    def _get_boto_resource(self):
        global cached_dynamo_resource
        if cached_dynamo_resource is None:
            logger.debug("Initialising boto3 dynamodb resource")
            cached_dynamo_resource = boto3.resource("dynamodb", os.getenv("REGION"))
        else:
            logger.debug("Using cached boto3 dynamodb resource")

        logger.info(f"Initialising DynamoDB table; name={self.name}")
        return cached_dynamo_resource.Table(self.name)

    @contextmanager
    def batch_writer(self):
        with self.boto_resource.batch_writer() as batch:
            yield DynamoDBTableBatchWriter(self, batch)

    def delete_item(self, *args, **kwargs):
        self._record_permission_use(DynamoDBTable.DELETE_ITEM_PERMISSION)
        return self.boto_resource.delete_item(*args, **kwargs)

    def get_item(self, *args, **kwargs):
        self._record_permission_use(DynamoDBTable.GET_ITEM_PERMISSION)
        return self.boto_resource.get_item(*args, **kwargs)

    def put_item(self, *args, **kwargs):
        self._record_permission_use(DynamoDBTable.PUT_ITEM_PERMISSION)
        return self.boto_resource.put_item(*args, **kwargs)

    def query(self, *args, **kwargs):
        additional_info = {"IndexName": kwargs["IndexName"]} if "IndexName" in kwargs else None
        self._record_permission_use(DynamoDBTable.QUERY_PERMISSION, additional_info=additional_info)
        return self.boto_resource.query(*args, **kwargs)

    def scan(self, *args, **kwargs):
        self._record_permission_use(DynamoDBTable.SCAN_PERMISSION)
        return self.boto_resource.scan(*args, **kwargs)

    def update_item(self, *args, **kwargs):
        self._record_permission_use(DynamoDBTable.UPDATE_ITEM_PERMISSION)
        return self.boto_resource.update_item(*args, **kwargs)


class DynamoDBTableBatchWriter(AWSResource):
    def __init__(self, table: DynamoDBTable, batch_writer):
        self._table = table
        self._batch_writer = batch_writer
        super().__init__(f"{table.name}-BatchWriter")

    def _get_boto_resource(self):
        return self._table

    def delete_item(self, *args, **kwargs):
        self._table._record_permission_use(DynamoDBTable.BATCH_WRITE_ITEM_PERMISSION)
        return self._batch_writer.delete_item(*args, **kwargs)

    def put_item(self, *args, **kwargs):
        self._table._record_permission_use(DynamoDBTable.BATCH_WRITE_ITEM_PERMISSION)
        return self._batch_writer.put_item(*args, **kwargs)
