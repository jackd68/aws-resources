import os

import boto3

from aws_resources.aws_resource import AWSResource
from logger import get_logger

logger = get_logger(__name__)

cached_sns_client = None


class SNSClient(AWSResource):
    PERMISSION_PREFIX = "sns"
    PUBLISH_PERMISSION = f"{PERMISSION_PREFIX}:Publish"

    _instance = None

    def __init__(self):
        super().__init__(name="SNS")

    def _get_boto_resource(self):
        global cached_sns_client
        if cached_sns_client is None:
            logger.debug("Initialising boto3 SNS client")
            cached_sns_client = boto3.client("sns", os.getenv("REGION"))
        else:
            logger.debug("Using cached boto3 SNS client")

        return cached_sns_client

    def publish(self, *args, **kwargs):
        self._record_permission_use(SNSClient.PUBLISH_PERMISSION)
        return self.boto_resource.publish(*args, **kwargs)

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = SNSClient()

        return cls._instance

    @classmethod
    def clear_instance(cls):
        cls._instance = None
