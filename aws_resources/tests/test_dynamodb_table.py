from unittest import TestCase
from unittest.mock import patch, Mock

from aws_resources.dynamodb_table import DynamoDBTable
from aws_resources.test_utils.cache import clear_cached_boto_resources
from aws_resources.test_utils.patchers import patch_table


class DynamoDBTableTestCase(TestCase):
    def setUp(self) -> None:
        clear_cached_boto_resources()

    def tearDown(self) -> None:
        clear_cached_boto_resources()

    @patch(
        "aws_resources.dynamodb_table.boto3",
        Mock(resource=Mock(return_value=Mock(Table=Mock(return_value="MockTable")))),
    )
    def test_should_instantiate_correctly(self):
        table = DynamoDBTable("test_table")
        self.assertEqual("test_table", table.name)
        self.assertEqual("MockTable", table.boto_resource)

    def test_should_call_delete_item_correctly_and_record_permission_use(self):
        with patch_table() as (mock_table, _mock_batch_writer):
            table = DynamoDBTable("test_table")
            return_value = table.delete_item(Item={"id": "test_id"})

        mock_table.delete_item.assert_called_once_with(Item={"id": "test_id"})
        self.assertEqual("delete_item_return_value", return_value)
        self.assertEqual(table, table.permission_uses[0].aws_resource)
        self.assertEqual(DynamoDBTable.DELETE_ITEM_PERMISSION, table.permission_uses[0].permission_name)

    def test_should_call_get_item_correctly_and_record_permission_use(self):
        with patch_table() as (mock_table, _mock_batch_writer):
            table = DynamoDBTable("test_table")
            return_value = table.get_item(Item={"id": "test_id"})

        mock_table.get_item.assert_called_once_with(Item={"id": "test_id"})
        self.assertEqual("get_item_return_value", return_value)
        self.assertEqual(table, table.permission_uses[0].aws_resource)
        self.assertEqual(DynamoDBTable.GET_ITEM_PERMISSION, table.permission_uses[0].permission_name)

    def test_should_call_put_item_correctly_and_record_permission_use(self):
        with patch_table() as (mock_table, _mock_batch_writer):
            table = DynamoDBTable("test_table")
            return_value = table.put_item(Item={"id": "test_id"})

        mock_table.put_item.assert_called_once_with(Item={"id": "test_id"})
        self.assertEqual("put_item_return_value", return_value)
        self.assertEqual(table, table.permission_uses[0].aws_resource)
        self.assertEqual(DynamoDBTable.PUT_ITEM_PERMISSION, table.permission_uses[0].permission_name)

    def test_should_call_query_correctly_and_record_permission_use(self):
        with patch_table() as (mock_table, _mock_batch_writer):
            table = DynamoDBTable("test_table")
            return_value = table.query(IndexName="TestIndex", AnotherArg="another_arg")

        mock_table.query.assert_called_once_with(IndexName="TestIndex", AnotherArg="another_arg")
        self.assertEqual("query_return_value", return_value)
        self.assertEqual(table, table.permission_uses[0].aws_resource)
        self.assertEqual(DynamoDBTable.QUERY_PERMISSION, table.permission_uses[0].permission_name)
        self.assertEqual({"IndexName": "TestIndex"}, table.permission_uses[0].additional_info)

    def test_should_handle_no_index_name_when_calling_query(self):
        with patch_table() as (mock_table, _mock_batch_writer):
            table = DynamoDBTable("test_table")
            return_value = table.query(AnotherArg="another_arg")

        mock_table.query.assert_called_once_with(AnotherArg="another_arg")
        self.assertEqual("query_return_value", return_value)
        self.assertEqual(table, table.permission_uses[0].aws_resource)
        self.assertEqual(DynamoDBTable.QUERY_PERMISSION, table.permission_uses[0].permission_name)
        self.assertIsNone(table.permission_uses[0].additional_info)

    def test_should_call_scan_correctly_and_record_permission_use(self):
        with patch_table() as (mock_table, _mock_batch_writer):
            table = DynamoDBTable("test_table")
            return_value = table.scan(Arg="arg")

        mock_table.scan.assert_called_once_with(Arg="arg")
        self.assertEqual("scan_return_value", return_value)
        self.assertEqual(table, table.permission_uses[0].aws_resource)
        self.assertEqual(DynamoDBTable.SCAN_PERMISSION, table.permission_uses[0].permission_name)

    def test_should_call_update_item_correctly_and_record_permission_use(self):
        with patch_table() as (mock_table, _mock_batch_writer):
            table = DynamoDBTable("test_table")
            return_value = table.update_item(Arg="arg")

        mock_table.update_item.assert_called_once_with(Arg="arg")
        self.assertEqual("update_item_return_value", return_value)
        self.assertEqual(table, table.permission_uses[0].aws_resource)
        self.assertEqual(DynamoDBTable.UPDATE_ITEM_PERMISSION, table.permission_uses[0].permission_name)

    def test_should_call_batch_writer_put_item_and_record_permission_use(self):
        with patch_table() as (_mock_table, mock_batch_writer):
            table = DynamoDBTable("test_table")

            with table.batch_writer() as batch:
                return_value = batch.put_item(Item={"id": "test_id"})

            mock_batch_writer.put_item.assert_called_once_with(Item={"id": "test_id"})
            self.assertEqual("batch_writer_put_item_return_value", return_value)
            self.assertEqual(table, table.permission_uses[0].aws_resource)
            self.assertEqual(DynamoDBTable.BATCH_WRITE_ITEM_PERMISSION, table.permission_uses[0].permission_name)

    def test_should_call_batch_writer_delete_item_and_record_permission_use(self):
        with patch_table() as (_mock_table, mock_batch_writer):
            table = DynamoDBTable("test_table")

            with table.batch_writer() as batch:
                return_value = batch.delete_item(Item={"id": "test_id"})

            mock_batch_writer.delete_item.assert_called_once_with(Item={"id": "test_id"})
            self.assertEqual("batch_writer_delete_item_return_value", return_value)
            self.assertEqual(table, table.permission_uses[0].aws_resource)
            self.assertEqual(DynamoDBTable.BATCH_WRITE_ITEM_PERMISSION, table.permission_uses[0].permission_name)
