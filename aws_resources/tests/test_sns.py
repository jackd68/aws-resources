from unittest import TestCase

from aws_resources.sns import SNSClient
from aws_resources.test_utils.patchers import patch_sns


class SNSResourceTestCase(TestCase):
    def setUp(self) -> None:
        SNSClient.clear_instance()

    def tearDown(self) -> None:
        SNSClient.clear_instance()

    def test_should_instantiate_correctly(self):
        with patch_sns():
            sns_client = SNSClient.get_instance()

        self.assertEqual("SNS", sns_client.name)

    def test_should_call_publish_correctly_and_record_permission_use(self):
        with patch_sns() as boto_sns_client:
            sns_client = SNSClient.get_instance()
            response = sns_client.publish(Arg="arg_value")

            boto_sns_client.publish.assert_called_once_with(Arg="arg_value")
            self.assertEqual({"MessageId": "mock_message_id"}, response)
            self.assertEqual(sns_client, SNSClient.get_instance().permission_uses[0].aws_resource)
            self.assertEqual(SNSClient.PUBLISH_PERMISSION, SNSClient.get_instance().permission_uses[0].permission_name)
