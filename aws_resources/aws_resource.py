from contextlib import contextmanager
from typing import List


class AWSResource:
    """A base class for AWS resources implementing functionality for recording uses of the resource's permissions"""

    def __init__(self, name: str):
        self.name = name
        self.boto_resource = self._get_boto_resource()
        self.permission_uses: List[PermissionUse] = []

    def _get_boto_resource(self):
        """Get the boto3 resource used to interact with the AWS service, i.e a Table or Bucket"""
        raise NotImplementedError()

    def _record_permission_use(self, permission_name: str, additional_info: dict = None):
        """Record the use of one of this resource's permissions

        :param permission_name: The name of the permission used
        :param additional_info: Additional info about the permission use, e.g. the name of the index used by a DynamoDB
            query operation
        """
        new_permission_use = PermissionUse(
            aws_resource=self, permission_name=permission_name, additional_info=additional_info
        )
        self.permission_uses.append(new_permission_use)

        if GlobalPermissionUseRecorder.is_recording():
            GlobalPermissionUseRecorder.get_instance().record_permission_use(new_permission_use)


class PermissionUse:
    """A recorded instance of an AWSResource's permission being used"""

    def __init__(self, aws_resource: AWSResource, permission_name: str, additional_info: dict = None):
        """

        :param aws_resource: The resource whose permission was used
        :param permission_name: The name of the permission used
        :param additional_info: Additional info about the permission use, e.g. the name of the index used by a DynamoDB
            query operation
        """
        self.aws_resource = aws_resource
        self.permission_name = permission_name
        self.additional_info = additional_info

    def __eq__(self, other):
        return (
            self.aws_resource == other.aws_resource
            and self.permission_name == other.permission_name
            and self.additional_info == other.additional_info
        )

    def __str__(self):
        additional_info_str = (
            "" if self.additional_info is None else ", ".join([f"{k}={v}" for k, v in self.additional_info.items()])
        )

        return (
            f"Permission {self.permission_name} used on {self.aws_resource.name} of type "
            f"{type(self.aws_resource).__name__}. "
            f"{additional_info_str}"
        )


class GlobalPermissionUseRecorder:
    """Records all permissions used by all AWSResources"""

    _instance = None

    def __init__(self):
        self.permission_uses = []

    @property
    def unique_permission_uses(self):
        unique_permissions = []
        for permission_use in self.permission_uses:
            if not any(unique_permission == permission_use for unique_permission in unique_permissions):
                unique_permissions.append(permission_use)

        return unique_permissions

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = GlobalPermissionUseRecorder()

        return cls._instance

    @classmethod
    def clear_instance(cls):
        cls._instance = None

    @classmethod
    def is_recording(cls):
        return cls._instance is not None

    def record_permission_use(self, permission_use: PermissionUse):
        self.permission_uses.append(permission_use)


@contextmanager
def record_permission_uses():
    """Handles the set up and tear down of the GlobalPermissionUseRecorder"""
    recorder = GlobalPermissionUseRecorder.get_instance()
    yield
    recorder.clear_instance()
